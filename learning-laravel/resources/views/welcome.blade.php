<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Day 1</title>
  </head>

  <body>
    <center>
      <table>
        <tr>
          <th colspan="2">
            <h1>SanberBook</h1>
          </th>
        </tr>
        <tr>
          <th colspan="2">
            <h2>Social Media Developer Santai Berkualitas</h2>
          </th>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
          </td>
        </tr>
        <tr>
          <td align="center">
            <h3>Benefit Join di SanberBook</h3>
          </td>
          <td>
            <ul>
              <li>Mendapatkan motivasi dari sesama developer</li>
              <li>Sharing knowledge dari para mastah Sanber</li>
              <li>Dibuat oleh calon web developer terbaik</li>
            </ul>
          </td>
        </tr>

        <tr>
          <td align="center">
            <h3>Cara Bergabung ke SanberBook</h3>
          </td>
          <td>
            <ul>
              <li>Mengunjungi Website ini</li>
              <li>Mendaftar di <a href="register">Form Sign Up</a></li>
              <li>Selesai!</li>
            </ul>
          </td>
        </tr>
      </table>
    </center>
  </body>
</html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Day 1</title>
  </head>
  <body>
    <center>
      <form action="/welcome" method="POST">
        @csrf
        <table>
          <tr>
            <th colspan="3" align="left"><h1>Buat Account Baru!</h1></th>
          </tr>
          <tr>
            <td colspan="3"><h3>Sign Up Form</h3></td>
          </tr>
          <tr>
            <td><label for="">First Name</label></td>
            <td>:</td>
            <td>
              <input type="text" name="firstName"value="" />
            </td>
          </tr>

          <tr>
            <td><label for="lastName">Last Name</label></td>
            <td>:</td>
            <td><input type="text" name="lastName" value="" /></td>
          </tr>

          <tr>
            <td><label for="gender">Gender</label></td>
            <td>:</td>
            <td>
              <input type="radio" name="gender" id="gender" value="Male" />Male
              <br />
              <input
                type="radio"
                name="gender"
                id="gender"
                value="Female"
              />Female
            </td>
          </tr>

          <tr>
            <td><label for="nationality">Nationality</label></td>
            <td>:</td>
            <td>
              <select>
                <optgroup label="Nationality">
                  <option value="Indonesian">Indonesian</option>
                  <option value="America">America</option>
                  <option value="England">England</option>
                </optgroup>
              </select>
            </td>
          </tr>

          <tr>
            <td><label for="languageSpoken">Language Spoken</label></td>
            <td>:</td>
            <td>
              <input
                type="checkbox"
                name="languageSpoken"
                id="languageSpoken"
                value="Bahasa Indonesia"
              />
              Bahasa Indonesia
              <br />
              <input
                type="checkbox"
                name="languageSpoken"
                id="languageSpoken"
                value="English"
              />
              English
              <br />
              <input
                type="checkbox"
                name="languageSpoken"
                id="languageSpoken"
                value="Other"
              />
              Other
            </td>
          </tr>

          <tr>
            <td><label for="bio">Bio</label></td>
            <td>:</td>
            <td><textarea name="" id="" cols="18" rows="5"></textarea></td>
          </tr>

          <tr>
            <td colspan="3" align="left">
              <button type="submit">Sign Up</button>
            </td>
          </tr>
        </table>
      </form>
    </center>
  </body>
</html>
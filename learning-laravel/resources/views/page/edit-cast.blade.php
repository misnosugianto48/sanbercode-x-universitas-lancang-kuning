@extends('layouts.master')

@section('title')
    Halaman Data Casts
@endsection

@section('content')
@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="/casts/{{$casts->id}}" method="POST">
  @method('PUT')
  @csrf
  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" name="name" value="{{$casts->name}}">
  </div>
  <div class="form-group">
    <label for="age">Age</label>
    <input type="number" class="form-control" id="" name="age" value="{{$casts->age}}">
  </div>
  <div class="form-group">
    <label for="bio">Bio</label>
    <textarea class="form-control" id="" rows="3" name="bio">{{$casts->bio}}</textarea>
  </div>
  <button type="submit" class="btn btn-outline-warning">Edit</button>
</form>

@endsection

@endsection

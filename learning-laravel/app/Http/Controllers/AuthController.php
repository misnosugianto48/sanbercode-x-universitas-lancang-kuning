<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function show()
    {
        return view('page.register');
    }

    public function register(Request $req)
    {
        $firstName = $req->input('firstName');
        $lastName = $req->input('lastName');

        return view('page.welcome', ['firstName' => $firstName, 'lastName' => $lastName]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $casts = Cast::all();
        return view('page.casts', ['casts' => $casts]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        return view('page.add-cast');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => 'required|min:5',
                'age' => 'required|min:2',
                'bio' => 'required|min:10'
            ],
            [
                'name.required' => 'A name is required',
                'age.required' => 'A age is required',
                'bio.required' => 'A bio is required',
                'name.min' => 'A name is validate with min 5 character',
                'age.min' => 'A name is validate with min 2 character',
                'bio.min' => 'A name is validate with min 10 character',
            ]
        );

        $cast = new Cast();

        $cast->name = $request->name;
        $cast->age = $request->age;
        $cast->bio = $request->bio;

        $cast->save();

        return redirect('/casts')->with('success', 'Data berhasil ditambah!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $casts = Cast::find($id);
        return view('page.edit-cast', ['casts' => $casts]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate(
            [
                'name' => 'required|min:5',
                'age' => 'required|min:2',
                'bio' => 'required|min:10'
            ],
            [
                'name.required' => 'A name is required',
                'age.required' => 'A age is required',
                'bio.required' => 'A bio is required',
                'name.min' => 'A name is validate with min 5 character',
                'age.min' => 'A name is validate with min 2 character',
                'bio.min' => 'A name is validate with min 10 character',
            ]
        );

        Cast::where('id', $id)
            ->update(
                [
                    'name' => $request->name,
                    'age' => $request->age,
                    'bio' => $request->bio
                ]
            );

        return redirect('/casts')->with('success', 'Data berhasil diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Cast::where('id', $id)
            ->delete();

        return redirect('/casts')->with('success', 'Data berhasil dihapus!');
    }
}

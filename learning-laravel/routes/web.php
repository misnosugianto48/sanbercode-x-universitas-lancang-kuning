<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', [HomeController::class, 'show']);

Route::get('/register', [AuthController::class, 'show']);
Route::post('/welcome', [AuthController::class, 'register']);

Route::get('/', function () {
  return view('layouts.master');
});

Route::get('/table', function () {
  return view('page.table');
});

Route::get('/data-table', function () {
  return view('page.data-table');
});

Route::resource('casts', CastController::class);

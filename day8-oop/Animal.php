<?php
class Animal
{
  public $name;
  public $legs = 4;
  public $coldBlooded = 'No';

  public function __construct($name)
  {
    $this->name = $name;
  }
}

<?php

require_once('./Animal.php');
require_once('./Frog.php');
require_once('./Ape.php');

$sheep = new Animal('Shaunn');
$monkey = new Ape('Alex');
$rudy = new Frog('Andy');

echo "This animal's name is of course  " . $sheep->name . "<br>";
echo "Shauun only has legs " . $sheep->legs . "<br>";
echo "Cold-blooded is this type of animal? " . $sheep->coldBlooded . "<br> <br>";

echo "This animal's name is of course  " . $monkey->name . "<br>";
echo "Alex only have legs " . $monkey->legs . "<br>";
echo "Cold-blooded is this type of animal? " . $monkey->coldBlooded . "<br>";
echo "This Animal has a jargon to jumping, yap that is " . $monkey->yell() . "<br>";

echo "This animal's name is of course  " . $rudy->name . "<br>";
echo "Andy only has legs " . $rudy->legs . "<br>";
echo "Cold-blooded is this type of animal? " . $rudy->coldBlooded . "<br>";
echo "When jumping, this animal has a sound like " . $rudy->jump() . "<br>";
